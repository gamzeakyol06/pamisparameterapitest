package Test;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import Base.Base;
import static io.restassured.RestAssured.given;

public class PUT_Device_Test extends Base {

    String token =doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();
    @BeforeTest()
    public void BeforeMethod(){

        map.put("id",67);
        map.put("name",generateRandomDataforName());
        map.put("WorkCenterID",80);
        map.put("deviceGUID","3fa85f64-5717-4562-b3fc-2c963f66afa6");
        map.put("additionalInfo", "info");
        map.put("isActive",true );

        System.out.println(map);
    }

    @Test(priority = 1,description = "200 Success")
    public void PUT_Update_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                body(map).
                when().
                put(DEVICE_PAGE_URL + "Device/Update?id="+67).
                then().
                statusCode(204).log().all();
    }


    @Test (priority = 2)
    public void PUT_Update_Assert_Test() throws InterruptedException, IOException {

        Response response = doGetRequest(DEVICE_PAGE_URL+"Device");
        List<Integer> jsonResponse_listid = doGetResponseListID(response);
        List<String> jsonResponse_name = doGetResponseName(response);

        for (int i = 0; i < jsonResponse_listid.size(); i++) {
            Integer postNameID = jsonResponse_listid.get(i);
            System.out.println(postNameID);
            if(postNameID == 67 ) {
                String NameData = jsonResponse_name.get(i);
                System.out.println(NameData);
                System.out.println("Name " + NameData);

                Assert.assertEquals(NameData, map.get("name"));
                System.out.println(NameData);
                System.out.println(map.get("name"));
            }
        }

    }

}
