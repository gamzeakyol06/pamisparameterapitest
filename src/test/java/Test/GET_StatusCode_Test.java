package Test;
import Base.Base;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class GET_StatusCode_Test extends Base {

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);

    // Device
    @Test
    public void Test_Device() {
        given().headers("Authorization","Bearer "+token).
                contentType("application/json").
                when().
                get(DEVICE_PAGE_URL + "Device").
                then().
                statusCode(200).log().all();
    }
}
