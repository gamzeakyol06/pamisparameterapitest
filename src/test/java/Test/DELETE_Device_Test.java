package Test;

import Base.Base;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static io.restassured.RestAssured.given;

public class DELETE_Device_Test extends Base {

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();
    @BeforeTest()
    public void BeforeMethod(){

    }

    @Test(description = "200 Success")
    public void Delete_Update_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                when().
                delete(DEVICE_PAGE_URL + "Device/Delete?id=66").
                then().
                statusCode(204).log().all();
    }
    @Test (priority = 2)
    public void Delete_Update_Assert_Test() throws InterruptedException, IOException {
        SoftAssert softassert = new SoftAssert();
        Response response = doGetRequest(DEVICE_PAGE_URL+"Device");
        List<Integer> jsonResponse_listid = doGetResponseListID(response);
        List<Boolean> jsonResponse_delete = doGetResponseisDelete(response);

        for (int i = 0; i < jsonResponse_listid.size(); i++) {
            Integer postNameID = jsonResponse_listid.get(i);
            System.out.println(postNameID);
            if(postNameID == 66) {
                Boolean postDeleteData = jsonResponse_delete.get(i);
                System.out.println(postDeleteData);
                System.out.println("Result" + postDeleteData);

                softassert.assertEquals(postDeleteData, map.get("isDelete"));
                System.out.println(postDeleteData);
                System.out.println(map.get("isDelete"));
            }
        }

    }
}
